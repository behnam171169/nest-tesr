/** @type {import('next').NextConfig} */
const withPWA = require("@ducanh2912/next-pwa").default({
  dest: "public",
  // disable: process.env.NODE_ENV === "development",
});

const nextConfig = {
  output: "standalone",
  async redirects() {
    return [
      {
        source: '/categoriesturn/مشاوره-فردی',
        destination: '/psychological-counseling/3-Individual-counseling',
        permanent: false
      },
    ]
  },
  reactStrictMode: false,
  experimental: { appDir: true },

  env: {
    // api: "http://localhost:3005",
    api: "https://rowshanravanbackend.iran.liara.run",
  },
  images: {
    domains: ["dl.rowshanravan.ir", "img.freepik.com"],
  },

  exportPathMap: function () {
    return {
      "/categoriesproduct/id": {
        page: "/categoriesproduct/[id]",
        query: { id: "id" },
      },
      "/products/id": {
        page: "/products/[id]",
        query: { id: "id" },
      },
      "/Lessons/id": {
        page: "/Lessons/[id]",
        query: { id: "id" },
      },
      "Admin/users/id": {
        page: "Admin/users/[id]",
        query: { id: "id" },
      },
      "Admin/teacherpay/id": {
        page: "Admin/teacherpay/[id]",
        query: { id: "id" },
      },
      "Admin/manageteacherproduct/id": {
        page: "Admin/manageteacherproduct/[id]",
        query: { id: "id" },
      },
      "/teacherproducts/id": {
        page: "/teacherproducts/[id]",
        query: { id: "id" },
      },
      "/clinicturn/id": {
        page: "/clinicturn/[id]",
        query: { id: "id" },
      },
      "/getturn/id": {
        page: "/getturn/[id]",
        query: { id: "id" },
      },
      "/onlineturn/id": {
        page: "/onlineturn/[id]",
        query: { id: "id" },
      },
      "/article/id": {
        page: "/article/[id]",
        query: { id: "id" },
      },
      "/workshopdetail/id": {
        page: "/workshopdetail/[id]",
        query: { id: "id" },
      },

      "/Admin/reservation/id": {
        page: "/Admin/reservation/[id]",
        query: { id: "id" },
      },
      "/Admin/edittrainingworkshop/id": {
        page: "/Admin/edittrainingworkshop/[id]",
        query: { id: "id" },
      },
      "/Admin/workshopstudents/id": {
        page: "/Admin/workshopstudents/[id]",
        query: { id: "id" },
      },
    };
  },
};

module.exports = nextConfig;
