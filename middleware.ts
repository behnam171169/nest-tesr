import { NextResponse } from 'next/server';
import type { NextRequest } from 'next/server';
import { URLfix } from "@persian-tools/persian-tools";
export async function middleware(request: NextRequest) {
    const fullhref = URLfix(
        request.nextUrl.href
      );

    if (fullhref?.includes('/categoriesturn/مشاوره-فردی') || fullhref?.includes('/categoriesturn/مشاور-فردی')) {
        return NextResponse.redirect(new URL('/psychological-counseling/3-Individual-counseling', request.url));
    } else if(fullhref?.includes('/categoriesturn/مشاوره-خانواده')){
        return NextResponse.redirect(new URL('/psychological-counseling/1-Family-counseling', request.url));
    }else if(fullhref?.includes('/categoriesturn/مشاوره-ازدواج')){
        return NextResponse.redirect(new URL('/psychological-counseling/2-Marriage-counseling', request.url));
    }else if(fullhref?.includes('/categoriesturn/روانشناس-کودک')){
        return NextResponse.redirect(new URL('/psychological-counseling/4-Child-psychologist', request.url));
    }else if(fullhref?.includes('/categoriesturn/روانشناس-نوجوان')){
        return NextResponse.redirect(new URL('/psychological-counseling/5-Adolescent-psychologist', request.url));
    }else if(fullhref?.includes('/categoriesturn/زوجدرمانگر')){
        return NextResponse.redirect(new URL('/psychological-counseling/6-Couple-therapist', request.url));
    }else if(fullhref?.includes('/categoriesturn/روانکاو')){
        return NextResponse.redirect(new URL('psychological-counseling/9-Psychoanalyst', request.url));
    }else if(fullhref?.includes('/categoriesturn/روان-درمانگر')){
        return NextResponse.redirect(new URL('/psychological-counseling/8-Psychotherapist', request.url));
    }else if(fullhref?.includes('/categoriesturn/سکس-تراپیست')){
        return NextResponse.redirect(new URL('/psychological-counseling/10-Sex-therapist', request.url));
    }else if(fullhref?.includes('/categoriesturn/روانشناس-بالینی')){
        return NextResponse.redirect(new URL('/psychological-counseling/7-Clinical-psychologist', request.url));
    }else if(fullhref?.includes('/categoriesturn/مشاور-شغلی')){
        return NextResponse.redirect(new URL('/psychological-counseling/11-Career-counseling', request.url));
    }else if(request.nextUrl.pathname =='/getturn/contact-us'){
        return NextResponse.redirect(new URL('/contact-us', request.url));
    }else if(fullhref?.includes('/getturn/مشاوره-آنلاین')){
        return NextResponse.redirect(new URL('/online-consultation', request.url));
    }else if(fullhref?.includes('/getturn/مشاوره-حضوری')){
        return NextResponse.redirect(new URL('/face-to-face-consultation', request.url));
    }else if(fullhref?.includes('auth/contact-us')){
        return NextResponse.redirect(new URL('/contact-us', request.url));
    }else if(fullhref?.includes('/auth/auth/forgetpassword')){
        return NextResponse.redirect(new URL('/auth/login', request.url));
    }else if(fullhref?.includes('/getturn/categoriesturn/مشاور-ترک-اعتیاد')){
        return NextResponse.redirect(new URL('psychological-counseling/14-Addiction-counseling', request.url));
    }else if(fullhref?.includes('/teacherproducts/سهیلا-قلی-پور-20605' )){
        return NextResponse.redirect(new URL('/teacherproducts/سهیلا-قلی-پور-20607', request.url));

    }


  
}