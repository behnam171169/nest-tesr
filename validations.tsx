import * as Yup from "yup";

export const userSchema = Yup.object().shape({
  name: Yup.string().required("Username must not be empty"),
  email: Yup.string()
    .required("Email must not be empty")
    .email("Email format is incorrect"),
  number: Yup.string()
    .required("Number must not be empty")
    .length(11, "Number must be eleven digits")
    .matches(/09-?[0-9]{2}-?[0-9]{3}-?[0-9]{4}/, "Number format is incorrect"),
});

