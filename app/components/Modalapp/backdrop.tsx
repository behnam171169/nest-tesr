
type Iprops={
  onClick:()=> void
}

function Backdrop({onClick }: Iprops) {
  return (
    <div
      className="fixed z-40  top-0 left-0 bottom-0 h-[100vh] w-[100vw] bg-[#000000e1] "
      onClick={onClick}
    ></div>
  );
}

export default Backdrop;
