import { useEffect, useRef,ReactNode  } from "react";
import { motion, usePresence } from "framer-motion";
import { IoMdClose } from "react-icons/io";
import Backdrop from "./backdrop";
import { gsap } from "gsap";
const dropIn = {
  hidden: {
    y: "-100vh",
    opacity: 0,
  },
  visible: {
    y: "0px",
    opacity: 1,
    transition: {
      type: "spring",
      damping: 25,
      stiffness: 500,
      delay: 0.5,
    },
  },
};

type Iprops={
  classname?:string;
  modalopen:boolean;
  handleClose:()=> void;
  children:ReactNode 
}
function Modal({ handleClose, modalopen, children, classname = "" }: Iprops) {
  const [isPresent, safeToRemove] = usePresence();
  const reff = useRef(null);
  useEffect(() => {
    if (!isPresent) {
      gsap.to(reff.current, {
        opacity: 0,
        y: "100vh",
        onComplete: () => safeToRemove?.(),
      });
    }
  }, [isPresent, safeToRemove]);
  if (!modalopen) return null;
  return (
    <>
      <Backdrop onClick={handleClose}></Backdrop>

      <motion.div
        ref={reff}
        className={`h-auto fixed top-1/4  z-50 bg-white ${classname}`}
        onClick={(e) => e.stopPropagation()}
        variants={dropIn}
        initial="hidden"
        animate="visible"
        exit="exit"
      >
        <IoMdClose onClick={()=>handleClose()} size={20} className="cursor-pointer absolute top-[3px] right-[3px]"/>
        
        {children}
      </motion.div>
    </>
  );
}

export default Modal;
