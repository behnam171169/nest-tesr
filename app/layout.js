"use client"
import React from "react";
import "./../styles/globals.css";
import { store } from "./../redux/store";
import { Provider } from "react-redux";
import { redirect } from "next/navigation";
import 'react-toastify/dist/ReactToastify.css';
import Head from "next/head";
import { ToastContainer } from 'react-toastify'
// --------------------------------------------------------------------------------------------------------------------------------------------
function RootLayout({ children }) {
  return (
    <html lang="fa-IR" dir="ltr">
      <Provider store={store}>
        <body className="bg-white  scrollbar-thumb-white  scrollbar-track-[#f7f4ed] overflow-x-hidden">
          <div className="w-full  min-h-screen h-auto md:pt-[60px] bg-white">
            {children}
          </div>
          <ToastContainer />
        </body>
      </Provider>
    </html>
  );
}

export default RootLayout;
