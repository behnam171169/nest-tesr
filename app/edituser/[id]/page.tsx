"use client";
import { Formik, Form } from "formik";
import {
  useUpdateUserMutation,
  useGetUserByIdQuery,
} from "../../../redux/service/dummyData";
import { InputFormik, SimpleBtn } from "./../../components";
import { toast } from "react-toastify";
import { useSearchParams } from "next/navigation";
import { userSchema } from "../../../validations";

interface FormValues {
  name: string;
  email: string;
  number: string;
}

type paramtype = {
  params: {
    id: string;
  };
};
function EditUser({ params }: paramtype) {
  const { data, isError, isLoading } = useGetUserByIdQuery(params.id!);
  const [updateUser] = useUpdateUserMutation();

  if (isError) {
    return <h1>We got an error.</h1>;
  }

  if (isLoading) {
    return <h1>Loading...</h1>;
  }

  return (
    <div className="flex flex-col items-center">
      <div className="w-[300px] h-auto md:w-[400px] border-2 border-cyan-100 mt-20 md:mt-40 p-4 md:p-6">
        <Formik
          enableReinitialize
          initialValues={{
            name: data?.name ?? "",
            email: data?.email ?? "",
            number: data?.number ?? "",
          }}
          validationSchema={userSchema}
          onSubmit={async (values: FormValues) => {
            try {
              await updateUser({
                id: params.id!,
                updateuserdata: {
                  name: values.name,
                  email: values.email,
                  number: values.number,
                },
              }).unwrap();
              toast("Updated successfully", {
                type: "success",
                autoClose: 2500,
              });
            } catch (error) {
              toast("An error was encountered", {
                type: "error",
                autoClose: 2500,
              });
            }
          }}
        >
          {({ errors, touched }) => (
            <Form className="w-full flex flex-col">
              <InputFormik
                placeholder="Name"
                name="name"
                wrapperClassName="w-full"
                className="!h-11 w-full rounded-lg px-4"
              />
              {errors.name && touched.name ? <div>{errors.name}</div> : null}
              <InputFormik
                placeholder="Email"
                name="email"
                wrapperClassName="w-full"
                className="!h-11 w-full rounded-lg px-4 text-sm"
              />
              {errors.email && touched.email ? <div>{errors.email}</div> : null}
              <InputFormik
                placeholder="Number"
                name="number"
                wrapperClassName="w-full"
                className="!h-11 w-full rounded-lg px-4 text-sm"
              />
              {errors.number && touched.number ? (
                <div>{errors.number}</div>
              ) : null}
              <SimpleBtn type="submit" className="mx-auto">
                Submit
              </SimpleBtn>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}

export default EditUser;
