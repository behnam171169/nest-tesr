"use client";
import { FcPlus } from "react-icons/fc";
import { v4 as uuidv4 } from "uuid";
import { toast } from "react-toastify";
import { RiEdit2Fill } from "react-icons/ri";
import { MdDelete } from "react-icons/md";
import Link from "next/link";
import Swal from "sweetalert2";
import {
  useGetAllUsersQuery,
  useAddNewUserMutation,
  useDeleteUserMutation,
} from "../redux/service/dummyData";
import { InputFormik, Modal, SimpleBtn } from "./components";
import { Form, Formik } from "formik";
import { userSchema } from "../validations";
import { useState } from "react";

interface FormValues {
  name: string;
  email: string;
  number: string;
}

function Allproducts() {
  const [modalopen, setModalopen] = useState(false);
  const [start,setStart]=useState<number>(1)
  const { data, isError, isLoading,refetch } = useGetAllUsersQuery(start);
  const [deleteUser] = useDeleteUserMutation();
  const [addNewProduct] = useAddNewUserMutation();

  const swalsetting: any = {
    title: "Are you sure you want to delete these components?",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes, delete it!",
  };

  const deleteuser = (id: string) => {
    Swal.fire(swalsetting).then(async (result) => {
      if (result.isConfirmed) {
        try {
          await deleteUser(id).unwrap();
          toast("done successfully", { type: "success", autoClose: 2500 });
        } catch (error) {
          toast("An error was encountered", {
            type: "error",
            autoClose: 25000,
          });
        }
      }
    });
  };


  const nextusers=async()=>{
    setStart(start+8)
   try {
    refetch().unwrap();
   } catch (error) {
    toast("با خطا مواجه شد", { type: "error", autoClose: 25000 });
   }
   
  }

  const backusers=()=>{
    setStart(start-8)
    try {
      refetch().unwrap();
     } catch (error) {
      toast("با خطا مواجه شد", { type: "error", autoClose: 25000 });
     }
      
  }

  if (isError) {
    return <h1>we got an error</h1>;
  }

  if (isLoading) {
    return (
      <h1 className="text-center mt-4 font-bold text-md md:text-lg">
        Loading...
      </h1>
    );
  }
  return (
    <div className="relative w-full  max-w-[1280px] mx-auto  pb-6">
      <Modal
        handleClose={() => setModalopen(false)}
        modalopen={modalopen}
        classname={`w-[350px] right-[calc(50%-175px)] md:right-[calc(50%-200px)] md:w-[400px] p-6 rounded-lg`}
      >
        <Formik
          enableReinitialize
          initialValues={{ name: "", number: "", email: "" }}
          validationSchema={userSchema}
          onSubmit={async (values: FormValues) => {
            try {
              const newProduct = {
                id: uuidv4,
                name: values.name,
                email: values.email,
                number: values.number,
              };
              await addNewProduct(newProduct).unwrap();
              setModalopen(false);
              toast("done successfully", { type: "success", autoClose: 2500 });
            } catch (error) {
              toast("با خطا مواجه شد", { type: "error", autoClose: 25000 });
            }
          }}
        >
          <Form className="w-full flex flex-col">
            <InputFormik
              placeholder="name"
              name="name"
              wrapperClassName="w-full"
              className="!h-11 w-full rounded-lg bg-[#D9D9D9] px-4"
            />
            <InputFormik
              placeholder="email"
              name="email"
              wrapperClassName="w-full"
              className="!h-11 w-full rounded-lg bg-[#D9D9D9] px-4 text-sm"
            />
            <InputFormik
              placeholder="number"
              name="number"
              wrapperClassName="w-full"
              className="!h-11 w-full rounded-lg bg-[#D9D9D9] px-4 text-sm"
            />
            <SimpleBtn type="submit" className="mx-auto">
              submit
            </SimpleBtn>
          </Form>
        </Formik>
      </Modal>
      <div className="flex flex-col w-full ">
        <div className="flex flex-row flex-wrap min-h-[calc(100vh-110px)]">
        {data?.map((usersdata) => (
        <div
          key={usersdata.id}
          className="relative flex flex-col items-center justify-center w-[45%] mx-[2.5%] my-4 h-[45vw] md:w-[30%] md:h-[30vw] lg:w-[20%] lg:mx-[2.5%] lg:h-[20vw] xl:h-[250px] md:mx-[1.666666666666666%] bg-red-500 "
        >
          <span className="w-auto max-w-[80%] font-bold text-sm md:text-md lg:text-lg">
            {usersdata.name}
          </span>
          <span className="w-auto max-w-[80%] font-bold text-sm md:text-md lg:text-lg mt-2 break-words">
            {usersdata.email}
          </span>
          <Link href={`/edituser/${usersdata.id}`}>
            <RiEdit2Fill
              size={30}
              className="absolute left-0 bottom-0 cursor-pointer"
            />
          </Link>
          <MdDelete
            onClick={() => deleteuser(usersdata.id)}
            size={30}
            className="absolute right-0 bottom-0 cursor-pointer"
          />
        </div>
      ))}
        </div>
      
      <div className="flex flex-row mx-auto items-center mt-2">
        <SimpleBtn onClick={()=>nextusers()}>next</SimpleBtn>
        <input value={start==1?1:Math.floor(start/8)+1} className="w-[30px] text-center h-[30px] mx-2 border-black border-[1px] border-solid" />
        <SimpleBtn onClick={()=>backusers()}>back</SimpleBtn>
      </div>
      </div>
    
      <FcPlus
        onClick={() => setModalopen(true)}
        size={60}
        className="fixed z-10 right-4 top-[calc(100%-80px)] cursor-pointer"
      />
    </div>
  );
}

export default Allproducts;
