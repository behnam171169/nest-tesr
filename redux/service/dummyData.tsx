import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

interface User {
  id: string;
  name: string;
  email: string;
  number: string;
}

interface NewUser {
  name: string;
  email: string;
  number: string;
}

interface UpdateUserData {
  name: string;
  email: string;
  number: string;
}

export const productsApi = createApi({
  reducerPath: "productsApi",
  baseQuery: fetchBaseQuery({ baseUrl: "http://localhost:3001/" }),
  tagTypes: ["User"],
  endpoints: (builder) => ({
    getAllUsers: builder.query<User[],number>({
      query: (start:number) => `users?_start=${start}&_limit=8`,
      providesTags: (result) =>
        result
          ? [...result.map(({ id }) => ({ type: "User" as const, id })), "User"]
          : ["User"],
    }),

    // getUserById: builder.query<User, string>({
    //   query: (id) => `/users/${id}`,

    // }),

    getUserById: builder.query<User, string>({
      query: (id) => `/users/${id}`,
      providesTags: (_result, _error, id) => [{ type: "User", id }],
    }),

    addNewUser: builder.mutation<void, NewUser>({
      query: (newUser) => ({
        url: "/users",
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: newUser,
      }),
      invalidatesTags: ["User"],
    }),

    updateUser: builder.mutation<
      void,
      { id: string; updateuserdata: UpdateUserData }
    >({
      query: ({ id, updateuserdata }) => ({
        url: `/users/${id}`,
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        body: updateuserdata,
      }),
      invalidatesTags: ["User"],
    }),

    deleteUser: builder.mutation<void, string>({
      query: (id) => ({
        url: `/users/${id}`,
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
      }),
      invalidatesTags: ["User"],
    }),
  }),
});

export const {
  useGetAllUsersQuery,
  useGetUserByIdQuery,
  useAddNewUserMutation,
  useUpdateUserMutation,
  useDeleteUserMutation,
} = productsApi;

// getAllUsers: builder.query<User[], { start?: number }>({
//   query: (params) => {
//   const url = new URL('users', 'http://example.com');
//   if (params?.start) {
//   url.searchParams.set('start', params.start.toString());
//   }
//   return url.toString();
//   },
//   providesTags: (result) =>
//   result
//   ? [...result.map(({ id }) => ({ type: "User" as const, id })), "User"]
//   : ["User"],
//   }),
