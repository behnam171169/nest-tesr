import { createSlice } from "@reduxjs/toolkit";

type initialStatetype = {
  useanimation: boolean;
};

const initialState: initialStatetype = {
  useanimation: true,
};

const Homeslice = createSlice({
  name: "type",
  initialState,
  reducers: {
    setUseanimation: (state, action: { type: string; payload: boolean }) => {
      state.useanimation = action.payload;
    },
  },
});

export const { setUseanimation } = Homeslice.actions;

export default Homeslice.reducer;
